# Deployment Nuxt with Sentry

[Sentry login](https://sentry.io/organizations/9to5/)

TODO:
* Add RELEASE_LEVEL
* Add RELEASE

Setup below:

## Nuxt

1. Add repo in Sentry integrations
2. Enable metadata `heroku labs:enable runtime-dyno-metadata -a <<app>>`
3. Heroku set env `heroku config:set SENTRY_DSN=TODO SENTRY_ENVIRONMENT=staging -a app-staging
`
4. Add sentry to project using [the github page](https://github.com/getsentry/raven-ruby)
5. Setup project

In `nuxt.config.js`:
```javascript
{
  sentry: {
    config: {
      release: process.env.HEROKU_SLUG_COMMIT,
      environment: process.env.SENTRY_ENVIRONMENT
    }
  }
}
```

6. Setup Circle CI with sentry

```yaml
version: 2.1

commands:
  deploy-heroku:
    description: "Deploy to Heroku"
    parameters:
      app:
        type: string
    steps:
      - run:
          name: Deploy master to Heroku
          command: |
            git push https://heroku:$HEROKU_API_KEY@git.heroku.com/<< parameters.app >>.git HEAD:refs/heads/master

  sentry:
    description: "Notify Sentry of deployment"
    parameters:
      environment:
        type: string
      project:
        type: string
    steps:
      - checkout
      - run:
          name: Deploy new release
          command: |
            sentry-cli releases -p << parameters.project >> new ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> set-commits --auto ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> finalize ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> deploys ${CIRCLE_SHA1} new -e << parameters.environment >>

jobs:
  build:
    working_directory: ~/app
    docker:
      - image: circleci/node:8.11.3
    steps:
      - checkout
      - restore_cache:
          key: yarn-packages-{{ checksum "yarn.lock" }}
      - run:
          name: Install Dependencies
          command: yarn install --non-interactive
      - save_cache:
          key: yarn-packages-{{ checksum "yarn.lock" }}
          paths:
            - node_modules/
      - run: yarn run lint
      - run: yarn run build

  deploy-production:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "app-production"

  deploy-staging:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "app-staging"

  sentry-production:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: app-web
          environment: production

  sentry-staging:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: app-web
          environment: staging

workflows:
  version: 2
  build-deploy:
    jobs:
      - build
      - deploy-production:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: master
      - deploy-staging:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: develop
      - sentry-production:
          context: org-global
          requires:
            - deploy-production
          filters:
            branches:
              only: master
      - sentry-staging:
          context: org-global
          requires:
            - deploy-staging
          filters:
            branches:
              only: develop
```

7. Deploy!
