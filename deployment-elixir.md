# Deployment Elixir with Sentry

[Sentry login](https://sentry.io/organizations/9to5/)

Setup below:

## Elixir

1. Add repo in Sentry integrations
2. Enable metadata `heroku labs:enable runtime-dyno-metadata -a <<app>>`
3. Heroku set env `heroku config:set SENTRY_DSN=TODO RELEASE_LEVEL=staging -a app-staging
`
4. Add sentry to project using [the github page](https://github.com/getsentry/sentry-elixir)
5. Setup project

In `config/config.exs`:
```elixir
config :sentry,
  dsn: System.get_env("SENTRY_DSN"),
  included_environments: ~w(production staging),
  environment_name: System.get_env("RELEASE_LEVEL") || "development",
  release: System.get_env("HEROKU_SLUG_COMMIT") || "INVALID",
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  tags: %{env: "production"}
```

In `buildpack_config` edit this line:

```
hook_pre_compile="mix deps.compile sentry --force"
```


6. Setup Circle CI with sentry

```yaml
version: 2.1

commands:
  deploy-heroku:
    description: "Deploy to Heroku"
    parameters:
      app:
        type: string
    steps:
      - run:
          name: Deploy master to Heroku
          command: |
            git push https://heroku:$HEROKU_API_KEY@git.heroku.com/<< parameters.app >>.git HEAD:refs/heads/master

  sentry:
    description: "Notify Sentry of deployment"
    parameters:
      environment:
        type: string
      project:
        type: string
    steps:
      - checkout
      - run:
          name: Deploy new release
          command: |
            sentry-cli releases -p << parameters.project >> new ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> set-commits --auto ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> finalize ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> deploys ${CIRCLE_SHA1} new -e << parameters.environment >>

jobs:
  build:
    working_directory: ~/app
    docker:
      - image: circleci/elixir:1.8.1
        environment: MIX_ENV=test
      - image: circleci/postgres:10.6-alpine
        environment:
          POSTGRES_USER: postgres
          POSTGRES_DB: baby_test
          POSTGRES_PASSWORD: postgres
    steps:
      - checkout
      - run: mix local.hex --force
      - run: mix local.rebar --force
      - restore_cache:
          keys:
            - v1-mix-cache-{{ .Branch }}-{{ checksum "mix.lock" }}
            - v1-mix-cache-{{ .Branch }}
            - v1-mix-cache
      - restore_cache:
          keys:
            - v1-build-cache-{{ .Branch }}-{{ checksum "mix.lock" }}
            - v1-build-cache-{{ .Branch }}
            - v1-build-cache
      - run: mix do deps.get, compile
      - save_cache:
          key: v1-mix-cache-{{ .Branch }}-{{ checksum "mix.lock" }}
          paths: "deps"
      - save_cache:
          key: v1-mix-cache-{{ .Branch }}
          paths: "deps"
      - save_cache:
          key: v1-mix-cache
          paths: "deps"
      - save_cache:
          key: v1-build-cache-{{ .Branch }}-{{ checksum "mix.lock" }}
          paths: "_build"
      - save_cache:
          key: v1-build-cache-{{ .Branch }}
          paths: "_build"
      - save_cache:
          key: v1-build-cache
          paths: "_build"
      - run: dockerize -wait tcp://localhost:5432 -timeout 1m
      - run: mix format --check-formatted
      - run: mix test
      - store_test_results:
          path: tmp/test-results

  deploy-production:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "baby-production"

  deploy-staging:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "baby-staging"

  sentry-production:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: 24baby-api
          environment: production

  sentry-staging:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: 24baby-api
          environment: staging

workflows:
  version: 2
  build-deploy:
    jobs:
      - build
      - deploy-production:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: master
      - deploy-staging:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: develop
      - sentry-production:
          context: org-global
          requires:
            - deploy-production
          filters:
            branches:
              only: master
      - sentry-staging:
          context: org-global
          requires:
            - deploy-staging
          filters:
            branches:
              only: develop

```

7. Deploy!
