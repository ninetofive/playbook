# Deployment Rails with Sentry

[Sentry login](https://sentry.io/organizations/9to5/)

Setup below:

## Rails

1. Add repo in Sentry integrations
2. Enable metadata `heroku labs:enable runtime-dyno-metadata -a <<app>>`
3. Heroku set env `heroku config:set SENTRY_DSN=TODO RELEASE_LEVEL=staging -a app-staging
`
4. Add sentry to project using [the github page](https://github.com/getsentry/raven-ruby)
5. Setup project

In `app/initializers/sentry.rb`:
```ruby
Raven.configure do |config|
  config.environments = %w[production staging]
  config.release = ENV['HEROKU_SLUG_COMMIT']
  config.current_environment = ENV['RELEASE_LEVEL'] || 'development'
end
```

6. Setup Circle CI with sentry

```yaml
version: 2.1

commands:
  deploy-heroku:
    description: "Deploy to Heroku"
    parameters:
      app:
        type: string
    steps:
      - run:
          name: Deploy master to Heroku
          command: |
            git push https://heroku:$HEROKU_API_KEY@git.heroku.com/<< parameters.app >>.git HEAD:refs/heads/master
  sentry:
    description: "Notify Sentry of deployment"
    parameters:
      environment:
        type: string
      project:
        type: string
    steps:
      - checkout
      - run:
          name: Deploy new release
          command: |
            sentry-cli releases -p << parameters.project >> new ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> set-commits --auto ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> finalize ${CIRCLE_SHA1}
            sentry-cli releases -p << parameters.project >> deploys ${CIRCLE_SHA1} new -e << parameters.environment >>

jobs:
  build:
    working_directory: ~/app
    docker:
      - image: circleci/ruby:2.6.3-node
        environment:
          RAILS_ENV: test
          PGUSER: nummi
          PGHOST: 127.0.0.1
      - image: postgres:10.4-alpine
        environment:
          POSTGRES_USER: nummi
          POSTGRES_DB: nummi_test
          POSTGRES_PASSWORD: ""
      - image: redis:4.0-alpine

    steps:
      - checkout
      - restore_cache:
          keys:
            - bundler-cache-{{ checksum "Gemfile.lock" }}
      - run: bundle install --path vendor/bundle
      - save_cache:
          key: bundler-cache-{{ checksum "Gemfile.lock" }}
          paths: "vendor/bundle"
      - run: dockerize -wait tcp://localhost:5432 -timeout 1m
      - run: bundle exec rake db:migrate
      - run: bundle exec rspec -r rspec_junit_formatter --format progress --format RspecJunitFormatter -o test-results/rspec.xml
      - store_test_results:
          path: test-results

  deploy-production:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "app-production"

  deploy-staging:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - deploy-heroku:
          app: "app-staging"

  sentry-production:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: <<todo-sentry-project-name-here>>
          environment: production

  sentry-staging:
    docker:
      - image: getsentry/sentry-cli
    steps:
      - checkout
      - sentry:
          project: <<todo-sentry-project-name-here>>
          environment: staging

workflows:
  version: 2
  build-deploy:
    jobs:
      - build
      - deploy-production:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: master
      - deploy-staging:
          context: org-global
          requires:
            - build
          filters:
            branches:
              only: develop
      - sentry-production:
          context: org-global
          requires:
            - deploy-production
          filters:
            branches:
              only: master
      - sentry-staging:
          context: org-global
          requires:
            - deploy-staging
          filters:
            branches:
              only: develop
```

7. Deploy!
